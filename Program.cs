﻿namespace UniversityLaboratory2._3
{
    class Program
    {
        private static void Main()
        {
            ConsoleMessageGenerator.GenerateLaboratoryTitle(
                new LaboratoryInfo(
                    "Бродский Егор Станиславович",
                    "ИР-133Б",
                    3,
                    "Керуючі оператори умовного та безумовного переходів."
                )
            );
            Console.WriteLine();
            TaskChooser.Run(new TaskInfo[]
                {
                    new TaskInfo("Завдання 1.", () =>
                    {
                        Console.WriteLine("type X");
                        var x = Utils.ToInt(Console.ReadLine());
                        Console.WriteLine("type Y");
                        var y = Utils.ToInt(Console.ReadLine());
                        Console.WriteLine("type Z");
                        var z = Utils.ToInt(Console.ReadLine());
                        try
                        {
                            var baseInt = Math.Pow(x, 2) + z;
                            if (baseInt == 0)
                            {
                                throw new DivideByZeroException();
                            }

                            var result = Math.Min(z, Math.Max(x, y)) / baseInt;
                            Console.WriteLine($"Result: {result}");
                        }
                        catch (Exception e)
                        {
                            if (e is DivideByZeroException)
                            {
                                Console.WriteLine("Divide by zero");
                            }
                            else
                            {
                                throw;
                            }
                        }

                        return null;
                    }),
                    new TaskInfo("Завдання 2.", () =>
                    {
                        Console.WriteLine("type X");
                        var x = Utils.ToFloatInt(Console.ReadLine());
                        Console.WriteLine("type Y");
                        var y = Utils.ToFloatInt(Console.ReadLine());
                        var n = -1;
                        if (x < 0 && y < 0)
                        {
                            n = 2;
                        }
                        else if (x < 0)
                        {
                            n = 1;
                        }
                        else if (y < 0)
                        {
                            n = 3;
                        }
                        else
                        {
                            n = 2;
                        }

                        Console.WriteLine($"Point({x};{y}) into N = {n}");
                        return null;
                    })
                }
            );
        }
    }
}